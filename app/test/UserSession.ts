import { Server } from "http";
import * as chai from "chai";
import { INestApplication } from "@nestjs/common";
import { getNestApplication } from "../index";
import { Response } from "superagent";

export class UserSession {
    private client: ChaiHttp.Agent = null;

    constructor () {}

    public async initialize (): Promise<void> {
        const nestApplication: INestApplication = await getNestApplication();
        const server: Server = nestApplication.getHttpServer();
        this.client = chai.request.agent(server);
    }

    public async getUsers (): Promise<Response> {
        return this.client.get("/api/users");
    }
}