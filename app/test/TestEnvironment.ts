import { UserSession } from "./UserSession";
import * as chai from "chai";
import chaiHttp = require("chai-http");

chai.use(chaiHttp);

export const user: UserSession = new UserSession();