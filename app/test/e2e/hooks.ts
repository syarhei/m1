import { user } from "../TestEnvironment";
import { initializeServer, startServer } from "../../index";

before("Init test ENVs", async function () {
    await initializeServer();
    await Promise.all([ user.initialize() ]);
    await startServer();
});