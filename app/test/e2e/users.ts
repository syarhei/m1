import { user } from "../TestEnvironment";
import { assert } from "chai";
import { Response } from "superagent";

describe("Users", function () {
    it("Get users", async function () {
        const response: Response = await user.getUsers();
        assert.deepEqual(response.status, 200);
        const users = response.body;
        assert.deepEqual(users, []);
    });
});