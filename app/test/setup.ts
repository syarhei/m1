import { load } from "dotenv";

load({ path: "test.env" });

import "./e2e/setup";