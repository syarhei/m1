import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { UserEntity } from "../models/data/UserEntity";
import { Repository } from "typeorm";
import { User } from "../models/contracts/User";
import { UserMapper } from "../mappers/UserMapper";

@Injectable()
export class UserRepository {
    constructor (
        @InjectRepository(UserEntity) private user: Repository<UserEntity>
    ) {}

    public async getUsers (): Promise<User[]> {
        const users: UserEntity[] = await this.user.find();
        return users.map(user => UserMapper.userEntityToUser(user));
    }
}