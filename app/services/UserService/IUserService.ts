import { User } from "./models/contracts/User";

export interface IUserService {
    getUsers (): Promise<User[]>;
}