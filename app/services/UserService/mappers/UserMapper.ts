import { UserEntity } from "../models/data/UserEntity";
import { User } from "../models/contracts/User";

export class UserMapper {
    public static userEntityToUser (entity: UserEntity): User {
        return {
            id: entity.id,
            first_name: entity.first_name,
            last_name: entity.last_name
        };
    }
}