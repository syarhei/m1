import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class UserEntity {
    @PrimaryGeneratedColumn("uuid")
    public id: string;
    @Column()
    public first_name: string;
    @Column()
    public last_name: string;
}