import { Module } from "@nestjs/common";
import { UserService } from "../UserService";
import { USER_REPOSITORY, USER_SERVICE } from "./ioc.id";
import { UserRepository } from "../DAL/UserRepository";
import { TypeOrmModule } from "@nestjs/typeorm";
import config from "../../../config";
import { UserEntity } from "../models/data/UserEntity";

@Module({
    imports: [
        TypeOrmModule.forRoot(config.db), TypeOrmModule.forFeature([ UserEntity ])
    ],
    providers: [
        { useClass: UserService, provide: USER_SERVICE },
        { useClass: UserRepository, provide: USER_REPOSITORY }
    ],
    exports: [
        { useClass: UserService, provide: USER_SERVICE }
    ]
})
export class UserModule {}