import { IUserService } from "./IUserService";
import { User } from "./models/contracts/User";
import { Inject, Injectable } from "@nestjs/common";
import { USER_REPOSITORY } from "./ioc/ioc.id";
import { UserRepository } from "./DAL/UserRepository";

@Injectable()
export class UserService implements IUserService {
    constructor (
        @Inject(USER_REPOSITORY) private userRepository: UserRepository
    ) {}

    public async getUsers (): Promise<User[]> {
        return this.userRepository.getUsers();
    }
}