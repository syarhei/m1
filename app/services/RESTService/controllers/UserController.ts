import { Controller, Get, HttpCode, Inject } from "@nestjs/common";
import { USER_SERVICE } from "../../UserService/ioc/ioc.id";
import { IUserService } from "../../UserService/IUserService";
import { User } from "../../UserService/models/contracts/User";
import { UserPresentationMapper } from "../mappers/UserPresentationMapper";
import { UserPresentation } from "../models/contracts/UserPresentation";

@Controller("/api/users")
export class UserController {
    constructor (
        @Inject(USER_SERVICE) private userService: IUserService
    ) {}

    @Get()
    @HttpCode(200)
    public async getUsers (): Promise<UserPresentation[]> {
        const users: User[] = await this.userService.getUsers();
        return users.map(user => UserPresentationMapper.userToUserPresentation(user));
    }
}