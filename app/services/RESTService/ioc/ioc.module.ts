import { Module } from "@nestjs/common";
import { UserModule } from "../../UserService/ioc/ioc.module";
import { UserController } from "../controllers/UserController";

@Module({
    imports: [ UserModule ],
    controllers: [ UserController ]
})
export class RESTModule {}