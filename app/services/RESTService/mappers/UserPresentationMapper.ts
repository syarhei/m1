import { User } from "../../UserService/models/contracts/User";
import { UserPresentation } from "../models/contracts/UserPresentation";

export class UserPresentationMapper {
    public static userToUserPresentation (user: User): UserPresentation {
        return {
            id: user.id,
            first_name: user.first_name,
            last_name: user.last_name
        };
    }
}