import { UserEntity } from "../services/UserService/models/data/UserEntity";
import { PostgresConnectionOptions } from "typeorm/driver/postgres/PostgresConnectionOptions";
import { SqlServerConnectionOptions } from "typeorm/driver/sqlserver/SqlServerConnectionOptions";

const databaseEntities: Function[] = [
    UserEntity
];

const databaseConnectionOptions: PostgresConnectionOptions | SqlServerConnectionOptions = {
    type: process.env.DB_TYPE as "postgres" & "mssql",
    host: process.env.DB_HOST,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    entities: databaseEntities,
    synchronize: true,
    logging: true,
    options: {
        encrypt: true
    }
};

const config = {
    web: {
        port: process.env.WEB_PORT
    },
    db: databaseConnectionOptions
};

export default config;