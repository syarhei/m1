import { load } from "dotenv";
load();
import { NestFactory } from "@nestjs/core";
import { RESTModule } from "./services/RESTService/ioc/ioc.module";
import { INestApplication, INestExpressApplication } from "@nestjs/common";
import config from "./config";

type Application = INestApplication & INestExpressApplication;

let application: Promise<Application> | Application = NestFactory.create(RESTModule);

async function initializeServer (): Promise<void> {
    application = await application;
}

async function startServer (): Promise<void> {
    application = await application;
    await application.listen(config.web.port);
    console.log("Server: started");
    console.log("Port:", config.web.port);
}

async function main (): Promise<void> {
    await initializeServer();
    await startServer();
}

(async () => {
    try {
        if (process.mainModule.filename === __filename) {
            await main();
        }
    } catch (err) {
        console.error(err);
    }
})();

async function getNestApplication (): Promise<Application> { return application; }

export { getNestApplication, initializeServer, startServer };